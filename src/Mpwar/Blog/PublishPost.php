<?php

namespace Mpwar\Blog;

use Mpwar\Blog\Exception\ValidationError;
use Mpwar\Blog\DataBase\PersistNewPost;
use Mpwar\Blog\NotificationQueue\NewPostEvent;

class PublishPost
{
    private $database;
    private $event;
    const MAXIMUM_TITLE_LENGHT = 50;
    const MAXIMUM_BODY_LENGHT = 1000;

    function __construct(PersistNewPost $database, NewPostEvent $event)
    {
        $this->database = $database;
        $this->event = $event;
    }


    public function __invoke($title, $body, $publishNow)
    {
        $TitleLenght = strlen ($title);
        $BodyLenght = strlen ($body);
        if($TitleLenght > self::MAXIMUM_TITLE_LENGHT){
            throw new ValidationError();
        }
        if($BodyLenght > self::MAXIMUM_BODY_LENGHT){
            throw new ValidationError();
        }
        $this->database->persist($title, $body);
        if($publishNow){
            $this->event->publishLastInsertedPost();
            return 'published';
        }
        return 'saved';
    }
}