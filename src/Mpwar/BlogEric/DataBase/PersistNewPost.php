<?php

// Do not modify this file at all

namespace Mpwar\BlogEric\DataBase;

interface PersistNewPost
{
    public function persist($title, $body);
}