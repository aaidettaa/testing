<?php

namespace Mpwar\BlogEric;

use Mpwar\BlogEric\DataBase\PersistNewPost;
use Mpwar\BlogEric\NotificationQueue\NewPostEvent;
use Mpwar\BlogEric\Validation\TextValidator;

class PublishPost
{
    const PUBLISH_NOW = 'meow';
    const PUBLISH_IN_THE_FUTURE = 'future';

    private $titleValidator;
    private $bodyValidator;
    private $persistNewPostRepository;
    private $newPostEventQueue;

    public function __construct(
        TextValidator $aTitleValidator,
        TextValidator $aBodyValidator,
        PersistNewPost $aPersistNewPostRepository,
        NewPostEvent $aNewPostEventQueue
    )
    {
        $this->titleValidator = $aTitleValidator;
        $this->bodyValidator = $aBodyValidator;
        $this->persistNewPostRepository = $aPersistNewPostRepository;
        $this->newPostEventQueue = $aNewPostEventQueue;
    }

    public function __invoke($title, $body, $publishNow)
    {
        $this->titleValidator->validateTextLength($title);
        $this->bodyValidator->validateTextLength($body);

        $this->persistNewPostRepository->persist($title, $body);

        if ($publishNow === self::PUBLISH_NOW) {
            $this->newPostEventQueue->publishLastInsertedPost();
        }
    }
}