<?php

namespace Mpwar\BlogEric\Validation\TextValidation;


use Mpwar\BlogEric\Exception\ValidationError;
use Mpwar\BlogEric\Validation\TextValidator;

final class BodyValidator implements TextValidator
{
    const BODY_SIZE_LIMIT = 1000;

    public function validateTextLength($text)
    {
        if (mb_strlen($text) > self::BODY_SIZE_LIMIT) {
            throw new ValidationError;
        }
    }
}