<?php

namespace Mpwar\BlogEric\Validation\TextValidation;

use Mpwar\BlogEric\Exception\ValidationError;
use Mpwar\BlogEric\Validation\TextValidator;

final class TitleValidator implements TextValidator
{
    const TITLE_SIZE_LIMIT = 50;

    public function validateTextLength($text)
    {
        if (mb_strlen($text) > self::TITLE_SIZE_LIMIT) {
            throw new ValidationError;
        }
    }
}