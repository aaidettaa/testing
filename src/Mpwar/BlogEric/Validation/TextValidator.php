<?php

namespace Mpwar\BlogEric\Validation;


interface TextValidator {

    public function validateTextLength($text);
}