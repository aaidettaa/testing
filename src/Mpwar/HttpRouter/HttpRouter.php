<?php

namespace Mpwar\HttpRouter;

class HttpRouter {

    const INITIAL_VALUE = null;
    private $routes;

    public function __construct($inputRoutes)
    {
        $this->routes = $inputRoutes;
    }

    public function getResult($inputRoute)
    {
        $stackedResult = self::INITIAL_VALUE;

        $InputRouteParts = explode("/", $inputRoute);

        foreach($this->routes as $key => $value){
            $RouteParts = explode("/", $value['path']);
            $count = 0;
            $params = array();
            $copyInputRoute = $inputRoute;
            if(count($InputRouteParts) <> count($RouteParts)){
                continue;
            }
            foreach($RouteParts as $part){
                if($this->isParameter($part)){
                    $paramName = $this->getParamName($part);
                    $copyInputRoute = str_replace($InputRouteParts[$count],$part,$copyInputRoute);
                    array_push($params, [
                        $paramName => $InputRouteParts[$count]
                    ]);
                }
                $count = $count + 1;
            }
            if($value['path'] === $copyInputRoute){
                if(count($params) >= 1){
                    return [
                        $inputRoute => [
                            'route_id' => $key,
                            'parameters' => $params[0]
                        ]
                    ];
                }else{
                    return [
                        $inputRoute => [
                            'route_id' => $key
                        ]
                    ];
                }
            }
        }

        return $stackedResult;
    }

    private function getParamName($InputPart){
        $result = $InputPart;
        if($this->startsWith($InputPart, "{") && $this->endsWith($InputPart, "}")){
            $result = str_replace("{","",$InputPart);
            $result = str_replace("}","",$result);
        }
        return $result;
    }

    private function isParameter($InputPart){
        if($this->startsWith($InputPart, "{") && $this->endsWith($InputPart, "}")){
            return true;
        }
        return false;
    }

    private function startsWith($haystack, $needle) {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    private function endsWith($haystack, $needle) {
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }
}