<?php

namespace Mpwar\HttpRouterEric\Component\ParameterExtractor;

use Mpwar\HttpRouterEric\Component\RouteParameterExtractor;
use Mpwar\HttpRouterEric\Route;

final class ExtractParametersFromPath implements RouteParameterExtractor
{
    private $knownRoutes;

    public function __construct(array $allKnownRoutes)
    {
        $this->knownRoutes = $allKnownRoutes;
    }

    public function extractParametersFor(Route $route)
    {
        $path = $route->path();
        $routePattern = $this->knownRoutes[$route->id()];
        $regex = $this->fromPatternToRegex($routePattern);

        $this->extractAndAddParametersToRoute($route, $path, $regex);
        return $route;
    }

    private function fromPatternToRegex($pattern)
    {
        return '@^' . preg_replace('/{(.+?)}/', '(?<$1>.+?)', $pattern) . '$@';
    }

    private function extractAndAddParametersToRoute(Route $route, $path, $regex)
    {
        preg_match($regex, $path, $matches);

        foreach ($matches as $key => $value) {
            if (is_numeric($key)) {
                continue;
            }

            $route->withParameter($key, $value);
        }
    }
}