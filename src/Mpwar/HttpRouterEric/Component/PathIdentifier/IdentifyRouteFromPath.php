<?php

namespace Mpwar\HttpRouterEric\Component\PathIdentifier;

use Mpwar\HttpRouterEric\Component\RoutePathIdentifier;
use Mpwar\HttpRouterEric\Route;

final class IdentifyRouteFromPath implements RoutePathIdentifier
{
    private $knownRoutes;

    public function __construct(array $allKnownRoutes)
    {
        $this->knownRoutes = $allKnownRoutes;
    }

    public function identifyPathFor(Route $route, $path)
    {
        foreach ($this->knownRoutes as $identifier => $pattern) {
            $routeRegex = $this->fromPatternToRegex($pattern);
            $pathMatchesRoute = $this->doesPathMatchesGivenRegex($path, $routeRegex);

            if ($pathMatchesRoute) {
                $route
                    ->withIdentifier($identifier)
                    ->withPath($path);

                break;
            }
        }

        return $route;
    }

    private function fromPatternToRegex($pattern)
    {
        return '@^' . preg_replace('/{.+?}/', '.+?', $pattern) . '$@';
    }

    private function doesPathMatchesGivenRegex($path, $regex)
    {
        return preg_match($regex, $path);
    }
}