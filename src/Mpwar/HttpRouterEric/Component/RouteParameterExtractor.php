<?php

namespace Mpwar\HttpRouterEric\Component;

use Mpwar\HttpRouterEric\Route;

interface RouteParameterExtractor
{
    public function extractParametersFor(Route $route);
}