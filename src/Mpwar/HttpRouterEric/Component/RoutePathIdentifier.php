<?php

namespace Mpwar\HttpRouterEric\Component;

use Mpwar\HttpRouterEric\Route;

interface RoutePathIdentifier
{
    public function identifyPathFor(Route $route, $path);
}