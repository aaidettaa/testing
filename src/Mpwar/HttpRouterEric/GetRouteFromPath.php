<?php

namespace Mpwar\HttpRouterEric;


use Mpwar\HttpRouterEric\Component\RoutePathIdentifier;
use Mpwar\HttpRouterEric\Component\RouteParameterExtractor;

class GetRouteFromPath
{
    private $routeIdentifier;
    private $parameterExtractor;

    public function __construct(
        RoutePathIdentifier $aRouteIdentifier,
        RouteParameterExtractor $aParameterExtractor
    ) {
        $this->routeIdentifier = $aRouteIdentifier;
        $this->parameterExtractor = $aParameterExtractor;
    }

    public function __invoke($path)
    {
        $route = Route::toBeFilled();
        $route = $this->routeIdentifier->identifyPathFor($route, $path);

        if ($route->isUnknown()) {
            throw UnrecognizedPath::create($path);
        }

        return $this->parameterExtractor->extractParametersFor($route);
    }
}