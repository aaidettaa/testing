<?php

namespace Mpwar\HttpRouterEric;

final class Route
{
    private $identifier;
    private $path;
    private $parameters;

    public function __construct()
    {
        $this->parameters = [];
    }

    public static function toBeFilled()
    {
        return new self;
    }

    public function withIdentifier($anIdentifier)
    {
        $this->identifier = $anIdentifier;

        return $this;
    }

    public function withPath($aPath)
    {
        $this->path = $aPath;

        return $this;
    }

    public function withParameter($parameter, $value)
    {
        $this->parameters[$parameter] = $value;
    }

    public function isUnknown()
    {
        return is_null($this->identifier);
    }

    public function id()
    {
        return $this->identifier;
    }

    public function path()
    {
        return $this->path;
    }

    public function parameters()
    {
        return $this->parameters;
    }
}