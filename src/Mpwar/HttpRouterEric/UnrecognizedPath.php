<?php

namespace Mpwar\HttpRouterEric;

use RuntimeException;

final class UnrecognizedPath extends RuntimeException
{
    public static function create($path)
    {
        return new self(sprintf(
            'The given path %s does not match with any existing route',
            $path
        ));
    }
}