<?php

namespace Mpwar\Money;


class Money
{

    const INITIAL_VALUE = 0;
    private $array_values = array();

    public function __construct($array_values)
    {
        $this->array_values = $array_values;
    }

    public function calculateResult($inputValue)
    {
        $resultArray = self::INITIAL_VALUE;
        $copy_value = $inputValue;
        foreach ($this->array_values as $element) {
            $number = $element->howManyTimesCanBeRepeated($copy_value);
            //N = N % 500 #Dinero restante
            $copy_value = $copy_value % $number;
        }
        return $resultArray;
    }
}