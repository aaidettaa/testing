<?php

namespace Mpwar\Money\Values;


class MoneyValue {

    private $value;

    function __construct($idValue)
    {
        $this->value = $idValue;
    }

    public function value(){
        return $this->value;
    }

    public function howManyTimesCanBeRepeated($inputValue){
        return  intval ($inputValue / $this->value);
    }
}