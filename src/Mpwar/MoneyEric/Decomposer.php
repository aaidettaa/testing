<?php

namespace Mpwar\MoneyEric;


class Decomposer
{
    private $decomposingSteps;

    public function __construct(DecomposingStepsCollection $allDecomposingSteps)
    {
        $this->decomposingSteps = $allDecomposingSteps;
    }

    public function __invoke($anAmount)
    {
        $stackedResult = [];
        $remainingAmount = $anAmount;

        foreach ($this->decomposingSteps as $decomposingStep) {
            $stepResult = $decomposingStep->computeStackedResultFor(
                $remainingAmount,
                $stackedResult
            );

            $stackedResult = $stepResult['stacked_result'];
            $remainingAmount = $stepResult['remaining_amount'];
        }

        return $stackedResult;
    }
}