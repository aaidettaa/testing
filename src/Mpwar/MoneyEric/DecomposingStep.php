<?php

namespace Mpwar\MoneyEric;


interface DecomposingStep
{
    public function computeStackedResultFor(
        $remainingAmount,
        array $stackedResult
    );
}