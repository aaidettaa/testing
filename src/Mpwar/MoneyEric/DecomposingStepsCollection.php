<?php

namespace Mpwar\MoneyEric;


use Iterator;

final class DecomposingStepsCollection implements Iterator
{
    private $steps;
    private $position;

    public function __construct()
    {
        $this->steps = [];
        $this->position = 0;
    }

    public function addStep(DecomposingStep $step)
    {
        $this->steps[] = $step;
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->steps[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->steps[$this->position]);
    }
}