<?php

namespace Mpwar\MoneyEric\Steps;


use Mpwar\MoneyEric\DecomposingStep;

final class GenericStep implements DecomposingStep
{
    private $unitValue;

    public function __construct($aUnitValue)
    {
        $this->unitValue = $aUnitValue;
    }

    public function computeStackedResultFor(
        $amount,
        array $stackedResult
    ) {
        $unitCountForTheGivenAmount = floor($amount / $this->unitValue);

        if ($unitCountForTheGivenAmount < 1) {
            return $this->generateReturnValue($amount, $stackedResult);
        }

        $remainingAmount = round($amount - $this->unitValue * $unitCountForTheGivenAmount, 2);
        $stackedResult[(string) $this->unitValue] = (int) $unitCountForTheGivenAmount;

        return $this->generateReturnValue($remainingAmount, $stackedResult);
    }

    private function generateReturnValue($remainingAmount, array $stackedResult)
    {
        return [
            'remaining_amount' => $remainingAmount,
            'stacked_result' => $stackedResult,
        ];
    }
}