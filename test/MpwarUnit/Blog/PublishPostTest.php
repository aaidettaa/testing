<?php

namespace MpwarUnit\Blog;


use Mpwar\Blog\DataBase\PersistNewPost;
use Mpwar\Blog\Exception\ValidationError;
use Mpwar\Blog\NotificationQueue\NewPostEvent;
use Mpwar\Blog\PublishPost;
use PHPUnit_Framework_TestCase;

class PublishPostTest extends PHPUnit_Framework_TestCase
{
    private $result = null;
    private $service = null;
    private $title = "";
    private $body = "";
    private $publishedNow = false;
    const MAXIMUM_TITLE_LENGHT = 50;
    const MAXIMUM_BODY_LENGHT = 1000;
    private $texto = <<<EOT
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Suspendisse suscipit in tellus at fringilla. Donec velit ligula,
molestie at sapien iaculis, vulputate lacinia velit. Vestibulum
in varius nibh, ut auctor mauris. Praesent fringilla lacus ac magna
vestibulum, in ultricies lorem pulvinar. Nulla tempor sagittis
turpis eget luctus. Integer vehicula eleifend maximus. Nunc vel sem
at nisi tincidunt commodo. In ullamcorper quam massa, vitae suscipit
tellus faucibus vitae. Donec neque ipsum, iaculis elementum efficitur
ut, consequat venenatis lacus. Maecenas molestie luctus aliquam.
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Suspendisse suscipit in tellus at fringilla. Donec velit ligula,
molestie at sapien iaculis, vulputate lacinia velit. Vestibulum
in varius nibh, ut auctor mauris. Praesent fringilla lacus ac magna
vestibulum, in ultricies lorem pulvinar. Nulla tempor sagittis
turpis eget luctus. Integer vehicula eleifend maximus. Nunc vel sem
at nisi tincidunt commodo. In ullamcorper quam massa, vitae suscipit
tellus faucibus vitae. Donec neque ipsum, iaculis elementum efficitur
ut, consequat venenatis lacus. Maecenas molestie luctus aliquam.
EOT;

    protected function tearDown()
    {
        $this->service = null;
        $this->result = null;
        $this->title = "";
        $this->body = "";
        $this->publishedNow = false;
    }

    /** @test */
    public function shouldReturnExceptionWhenTitleIsGreatenThanMaximum()
    {
        $this->givenPostThatTitleIsGreaterThanMaximum();
        $this->thenTheResultShouldBeAnException();
        $this->whenGettingThePublishPostResult();
    }

    /** @test */
    public function shouldReturnExceptionWhenBodyIsGreatenThanMaximum()
    {
        $this->givenPostThatBodyIsGreaterThanMaximum();
        $this->thenTheResultShouldBeAnException();
        $this->whenGettingThePublishPostResult();
    }

    /** @test */
    public function shouldBePersistedAndNotPublishedWhenNotPublishedNow()
    {
        $this->givenPostCorrectlyWithNotPublishedNow();
        $this->whenGettingThePublishPostResult();
        $this->thenTheResultShouldBePersisted();
    }

    /** @test */
    public function shouldBePersistedAndPublishedWhenPublishedNow()
    {
        $this->givenPostCorrectlyWithPublishedNow();
        $this->whenGettingThePublishPostResult();
        $this->thenTheResultShouldBePublished();
    }

    private function givenPostThatTitleIsGreaterThanMaximum()
    {
        $databaseMock = $this->getMock(PersistNewPost::class);
        $eventMock = $this->getMock(NewPostEvent::class);
        $this->title = "Lorem ipsum dolor sit amet, consecetur adipiscing elit. Suspendisse suscipit in tellus at fringilla.";
        $this->body = <<<EOT
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Suspendisse suscipit in tellus at fringilla. Donec velit ligula,
molestie at sapien iaculis, vulputate lacinia velit. Vestibulum
in varius nibh, ut auctor mauris. Praesent fringilla lacus ac magna
vestibulum, in ultricies lorem pulvinar. Nulla tempor sagittis
turpis eget luctus. Integer vehicula eleifend maximus. Nunc vel sem
at nisi tincidunt commodo. In ullamcorper quam massa, vitae suscipit
tellus faucibus vitae. Donec neque ipsum, iaculis elementum efficitur
ut, consequat venenatis lacus. Maecenas molestie luctus aliquam.
EOT;
        $this->publishedNow = false;
        $this->service = new PublishPost($databaseMock,$eventMock);
    }

    private function givenPostThatBodyIsGreaterThanMaximum()
    {
        $databaseMock = $this->getMock(PersistNewPost::class);
        $eventMock = $this->getMock(NewPostEvent::class);
        $this->title = "Lorem ipsum dolor sit amet, consectetur.";
        $this->body = <<<EOT
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Suspendisse suscipit in tellus at fringilla. Donec velit ligula,
molestie at sapien iaculis, vulputate lacinia velit. Vestibulum
in varius nibh, ut auctor mauris. Praesent fringilla lacus ac magna
vestibulum, in ultricies lorem pulvinar. Nulla tempor sagittis
turpis eget luctus. Integer vehicula eleifend maximus. Nunc vel sem
at nisi tincidunt commodo. In ullamcorper quam massa, vitae suscipit
tellus faucibus vitae. Donec neque ipsum, iaculis elementum efficitur
ut, consequat venenatis lacus. Maecenas molestie luctus aliquam.
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Suspendisse suscipit in tellus at fringilla. Donec velit ligula,
molestie at sapien iaculis, vulputate lacinia velit. Vestibulum
in varius nibh, ut auctor mauris. Praesent fringilla lacus ac magna
vestibulum, in ultricies lorem pulvinar. Nulla tempor sagittis
turpis eget luctus. Integer vehicula eleifend maximus. Nunc vel sem
at nisi tincidunt commodo. In ullamcorper quam massa, vitae suscipit
tellus faucibus vitae. Donec neque ipsum, iaculis elementum efficitur
ut, consequat venenatis lacus. Maecenas molestie luctus aliquam.
EOT;
        $this->publishedNow = false;
        $this->service = new PublishPost($databaseMock,$eventMock);
    }

    private function givenPostCorrectlyWithPublishedNow()
    {
        $databaseMock = $this->getMock(PersistNewPost::class);
        $eventMock = $this->getMock(NewPostEvent::class);
        $this->title = $this->cutString($this->texto, self::MAXIMUM_TITLE_LENGHT - 5);
        //$this->title = "Lorem ipsum dolor sit amet, consectetur.";
        $this->body = <<<EOT
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Suspendisse suscipit in tellus at fringilla. Donec velit ligula,
molestie at sapien iaculis, vulputate lacinia velit. Vestibulum
in varius nibh, ut auctor mauris. Praesent fringilla lacus ac magna
vestibulum, in ultricies lorem pulvinar. Nulla tempor sagittis
turpis eget luctus. Integer vehicula eleifend maximus. Nunc vel sem
at nisi tincidunt commodo. In ullamcorper quam massa, vitae suscipit
tellus faucibus vitae. Donec neque ipsum, iaculis elementum efficitur
ut, consequat venenatis lacus. Maecenas molestie luctus aliquam.
EOT;
        $this->publishedNow = true;
        $this->service = new PublishPost($databaseMock,$eventMock);
    }

    private function givenPostCorrectlyWithNotPublishedNow()
    {
        $databaseMock = $this->getMock(PersistNewPost::class);
        $eventMock = $this->getMock(NewPostEvent::class);
        $this->title = "Lorem ipsum dolor sit amet, consectetur.";
        $this->body = <<<EOT
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Suspendisse suscipit in tellus at fringilla. Donec velit ligula,
molestie at sapien iaculis, vulputate lacinia velit. Vestibulum
in varius nibh, ut auctor mauris. Praesent fringilla lacus ac magna
vestibulum, in ultricies lorem pulvinar. Nulla tempor sagittis
turpis eget luctus. Integer vehicula eleifend maximus. Nunc vel sem
at nisi tincidunt commodo. In ullamcorper quam massa, vitae suscipit
tellus faucibus vitae. Donec neque ipsum, iaculis elementum efficitur
ut, consequat venenatis lacus. Maecenas molestie luctus aliquam.
EOT;
        $publishedNow = false;
        $this->service = new PublishPost($databaseMock,$eventMock);
    }

    private function whenGettingThePublishPostResult()
    {
        $this->result = $this->service->__invoke($this->title, $this->body, $this->publishedNow);
    }

    private function thenTheResultShouldBeAnException()
    {
        $this->expectException(ValidationError::class);
    }

    private function thenTheResultShouldBePersisted()
    {
        $this->assertSame('saved', $this->result);
    }

    private function thenTheResultShouldBePublished()
    {
        $this->assertSame('published', $this->result);
    }

    private function cutString($initialString, $lenght){
        return substr ($initialString,0, $lenght );
    }
}