<?php

namespace MpwarUnit\BlogEric;

use PHPUnit_Framework_TestCase;
use Mpwar\BlogEric\PublishPost;
use Mpwar\BlogEric\Database\PersistNewPost;
use Mpwar\BlogEric\Exception\ValidationError;
use Mpwar\BlogEric\NotificationQueue\NewPostEvent;
use Mpwar\BlogEric\Validation\TextValidator;

final class PublishPostTest extends PHPUnit_Framework_TestCase
{
    const TITLE = 'post title';
    const BODY = 'entry body';

    protected function tearDown()
    {
        $this->title = null;
        $this->body = null;
        $this->persistNewPostRepository = null;
        $this->newPostEventQueue = null;
    }

    /** @test */
    public function shouldFailWithTooLargeTitle()
    {
        $this->givenATitleWithMoreThanFiftyCharacters();
        $this->andAnEntryBody();
        $this->thenAnErrorShouldBeThrown();
        $this->andNothingShouldBePersisted();
        $this->andNothingShouldBeNotifiedInTheQueue();
        $this->whenPublishingANewPostRightNow();
    }

    /** @test */
    public function shouldFailWithTooLargeBody()
    {
        $this->givenAnEntryTitle();
        $this->andABodyWithMoreThan1000Characters();
        $this->thenAnErrorShouldBeThrown();
        $this->andNothingShouldBePersisted();
        $this->andNothingShouldBeNotifiedInTheQueue();
        $this->whenPublishingANewPostRightNow();
    }

    /** @test */
    public function shouldPersistTheEntry()
    {
        $this->givenAnEntryTitle();
        $this->andAnEntryBody();
        $this->thenThePostShouldBePersisted();
        $this->andNothingShouldBeNotifiedInTheQueue();
        $this->whenPublishingANewPostInTheFuture();
    }

    /** @test */
    public function shouldPersistTheEntryAndPublishIt()
    {
        $this->givenAnEntryTitle();
        $this->andAnEntryBody();
        $this->thenThePostShouldBePersisted();
        $this->andTheQueueShouldBeNotified();
        $this->whenPublishingANewPostRightNow();
    }

    private function givenATitleWithMoreThanFiftyCharacters()
    {
        $this->title = self::TITLE;

        $this->titleValidator = $this->getMock(TextValidator::class);
        $this->titleValidator
            ->method('validateTextLength')
            ->will($this->throwException(new ValidationError));
    }

    private function givenAnEntryTitle()
    {
        $this->title = self::TITLE;

        $this->titleValidator = $this->getMock(
            TextValidator::class,
            ['validateTextLength']
        );
    }

    private function andAnEntryBody()
    {
        $this->body = self::BODY;

        $this->bodyValidator = $this->getMock(
            TextValidator::class,
            ['validateTextLength']
        );
    }

    private function andABodyWithMoreThan1000Characters()
    {
        $this->body = self::BODY;

        $this->bodyValidator = $this->getMock(TextValidator::class);
        $this->bodyValidator
            ->method('validateTextLength')
            ->will($this->throwException(new ValidationError));
    }

    private function thenAnErrorShouldBeThrown()
    {
        $this->setExpectedException(ValidationError::class);
    }

    private function andNothingShouldBePersisted()
    {
        $this->persistNewPostRepository = $this->getMock(PersistNewPost::class);
        $this->persistNewPostRepository
            ->expects($this->never())
            ->method('persist');
    }

    private function thenThePostShouldBePersisted()
    {
        $this->persistNewPostRepository = $this->getMock(PersistNewPost::class);
        $this->persistNewPostRepository
            ->expects($this->once())
            ->method('persist')
            ->with($this->title, $this->body);
    }

    private function andNothingShouldBeNotifiedInTheQueue()
    {
        $this->newPostEventQueue = $this->getMock(NewPostEvent::class);
        $this->newPostEventQueue
            ->expects($this->never())
            ->method('publishLastInsertedPost');
    }

    private function andTheQueueShouldBeNotified()
    {
        $this->newPostEventQueue = $this->getMock(NewPostEvent::class);
        $this->newPostEventQueue
            ->expects($this->once())
            ->method('publishLastInsertedPost');
    }

    private function whenPublishingANewPostRightNow()
    {
        $this->executeService(PublishPost::PUBLISH_NOW);
    }

    private function whenPublishingANewPostInTheFuture()
    {
        $this->executeService(PublishPost::PUBLISH_IN_THE_FUTURE);
    }

    private function executeService($when)
    {
        $publisPostService = new PublishPost(
            $this->titleValidator,
            $this->bodyValidator,
            $this->persistNewPostRepository,
            $this->newPostEventQueue
        );

        $publisPostService($this->title, $this->body, $when);
    }
}