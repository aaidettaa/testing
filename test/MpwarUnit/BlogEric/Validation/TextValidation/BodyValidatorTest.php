<?php

namespace MpwarUnit\BlogEric\Validation\TextValidation;

use PHPUnit_Framework_TestCase;
use Mpwar\BlogEric\Exception\ValidationError;
use Mpwar\BlogEric\Validation\TextValidation\BodyValidator;

final class BodyValidatorTest extends PHPUnit_Framework_TestCase
{
    const VALID_BODY = 'valid body';

    protected function tearDown()
    {
        $this->body = null;
        $this->result = null;
    }

    /** @test */
    public function shouldFailWithTooLongBodies()
    {
        $this->givenATooLongBody();
        $this->thenTheValidationShouldFail();
        $this->whenExecutingIt();
    }

    /** @test */
    public function shouldReturnNothingWithNormalBodies()
    {
        $this->givenAValidBody();
        $this->whenExecutingIt();
        $this->thenNothingShouldBeReturned();
    }

    private function givenATooLongBody()
    {
        $this->body = str_repeat(self::VALID_BODY, 101);
    }

    private function givenAValidBody()
    {
        $this->body = self::VALID_BODY;
    }

    private function thenTheValidationShouldFail()
    {
        $this->setExpectedException(ValidationError::class);
    }

    private function thenNothingShouldBeReturned()
    {
        $this->assertNull($this->result);
    }

    private function whenExecutingIt()
    {
        $bodyValidator = new BodyValidator;
        $this->result = $bodyValidator->validateTextLength($this->body);
    }
}