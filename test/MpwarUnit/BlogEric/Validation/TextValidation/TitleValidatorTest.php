<?php

namespace MpwarUnit\BlogEric\Validation\TextValidation;

use PHPUnit_Framework_TestCase;
use Mpwar\BlogEric\Exception\ValidationError;
use Mpwar\BlogEric\Validation\TextValidation\TitleValidator;

final class TitleValidatorTest extends PHPUnit_Framework_TestCase
{
    const VALID_TITLE = 'valid title';
    const TOO_LONG_TITLE = 'a too long title is the one that has more than 50 characters in it';

    protected function tearDown()
    {
        $this->title = null;
        $this->result = null;
    }

    /** @test */
    public function shouldFailWithTooLongTitles()
    {
        $this->givenATooLongTitle();
        $this->thenTheValidationShouldFail();
        $this->whenExecutingIt();
    }

    /** @test */
    public function shouldReturnNothingWithNormalTitles()
    {
        $this->givenAValidTitle();
        $this->whenExecutingIt();
        $this->thenNothingShouldBeReturned();
    }

    private function givenATooLongTitle()
    {
        $this->title = self::TOO_LONG_TITLE;
    }

    private function givenAValidTitle()
    {
        $this->title = self::VALID_TITLE;
    }

    private function thenTheValidationShouldFail()
    {
        $this->setExpectedException(ValidationError::class);
    }

    private function thenNothingShouldBeReturned()
    {
        $this->assertNull($this->result);
    }

    private function whenExecutingIt()
    {
        $titleValidator = new TitleValidator;
        $this->result = $titleValidator->validateTextLength($this->title);
    }
}