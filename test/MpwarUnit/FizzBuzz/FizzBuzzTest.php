<?php

namespace MpwarUnit\FizzBuzz;

use Mpwar\FizzBuzz\FizzBuzz;
use Mpwar\FizzBuzz\Solver;
use PHPUnit_Framework_TestCase;

final class FizzBuzzTest extends PHPUnit_Framework_TestCase
{
    const NUMBER = 24;
    const RESULT_FOR_ZERO_CASES = '';
    const FIZZ = 'fizz';
    const LAST_SOLVER_RETURN_VALUE = 'monesvol';

    protected function tearDown()
    {
        $this->solver = null;
        $this->result = null;
    }

    /** @test */
    public function shouldReturnEmptyStringWhenThereAreNoSolvers()
    {
        $this->givenThatThereAreNoSolvers();
        $this->whenGettingTheFizzBuzzResult();
        $this->thenTheResultShouldBeEmptyString();
    }

    /** @test */
    public function shouldGetTheResultForASingleSolver()
    {
        $this->givenThatIHaveASingleSolver();
        $this->whenGettingTheFizzBuzzResult();
        $this->thenTheResultShouldBeTheOneFromTheSingleSolver();
    }

    /** @test */
    public function shouldGetTheResultWhenIHaveMoreThanOneSolver()
    {
        $this->givenThatIHaveMoreThanOneSolver();
        $this->whenGettingTheFizzBuzzResult();
        $this->thenTheResultShouldBeTheOneFromTheLastSolver();
    }

    private function givenThatThereAreNoSolvers()
    {
        $this->solvers = [];
    }

    private function givenThatIHaveASingleSolver()
    {
        $singleSolver = $this->getMock(Solver::class);
        $singleSolver
            ->method('composeStackedResult')
            ->with(self::NUMBER, '')
            ->will($this->returnValue(self::FIZZ));

        $this->solvers = [$singleSolver];
    }

    private function givenThatIHaveMoreThanOneSolver()
    {
        $firstSolver = $this->getMock(Solver::class);
        $firstSolver
            ->method('composeStackedResult')
            ->will($this->returnValue(self::FIZZ));

        $secondSolver = $this->getMock(Solver::class);
        $secondSolver
            ->method('composeStackedResult')
            ->will($this->returnValue(self::FIZZ));

        $thirdSolver = $this->getMock(Solver::class);
        $thirdSolver
            ->method('composeStackedResult')
            ->will($this->returnValue(self::LAST_SOLVER_RETURN_VALUE));

        $this->solvers = [$firstSolver, $secondSolver, $thirdSolver];
    }

    private function whenGettingTheFizzBuzzResult()
    {
        $service = new FizzBuzz($this->solvers);
        $this->result = $service->calculateResult(self::NUMBER);
    }

    private function thenTheResultShouldBeEmptyString()
    {
        $this->assertSame(self::RESULT_FOR_ZERO_CASES, $this->result);
    }

    private function thenTheResultShouldBeTheOneFromTheSingleSolver()
    {
        $this->assertSame(self::FIZZ, $this->result);
    }

    private function thenTheResultShouldBeTheOneFromTheLastSolver()
    {
        $this->assertSame(self::LAST_SOLVER_RETURN_VALUE, $this->result);
    }
}