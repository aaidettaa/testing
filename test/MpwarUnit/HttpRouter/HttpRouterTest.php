<?php

namespace MpwarUnit\FizzBuzz;

use Mpwar\HttpRouter\HttpRouter;
use PHPUnit_Framework_TestCase;

final class HttpRouterTest extends PHPUnit_Framework_TestCase
{

    const VALUE_FOR_NO_COINCIDENCES = '/bar/1';
    const VALUE_FOR_COINCIDENCES = '/home';
    const VALUE_FOR_COMPLEX_COINCIDENCES = '/students/50/scores';
    const RESULT_FOR_NO_COINCIDENCES = null;
    private $inicialValue = "";
    private $routes = [
        'home_page' => [
            'path' => '/home'
        ],
        'students_resource' => [
            'path' => '/students/{id}'
        ],
        'students_scores_collection' => [
            'path' => '/students/{id}/scores'
        ]
    ];

    protected function tearDown()
    {
        $this->inicialValue = null;
        $this->result = null;
    }

    /** @test */
    public function shouldReturnNullWhenThereAreNoCoincidences()
    {
        $this->givenThatThereAreNoCoincidences();
        $this->whenGettingTheHttpRouterResult();
        $this->thenTheResultShouldBeNull();
    }
    /** @test */
    public function shouldReturnSimpleArrayWhenThereAreSimpleCoincidences()
    {
        $this->givenThatThereAreCoincidences();
        $this->whenGettingTheHttpRouterResult();
        $this->thenTheResultShouldBeAnArray();
        $this->thenTheResultShouldBeOneElement();
        $this->thenTheRouteResultShouldBeTheSameAsInicial();
        $this->thenTheResultShouldHaveRouteId();
    }
    /** @test */
    public function shouldReturnArrayWithParametersWhenThereAreComplexCoincidences()
    {
        $this->givenThatThereAreComplexCoincidences();
        $this->whenGettingTheHttpRouterResult();
        $this->thenTheResultShouldBeAnArray();
        $this->thenTheResultShouldBeOneElement();
        $this->thenTheRouteResultShouldBeTheSameAsInicial();
        $this->thenTheResultShouldHaveRouteId();
        $this->thenTheResultShouldHaveParameters();
        $this->thenTheParametersShouldBeAnArray();
    }

    private function givenThatThereAreNoCoincidences()
    {
        $this->inicialValue = self::VALUE_FOR_NO_COINCIDENCES;
    }

    private function givenThatThereAreCoincidences()
    {
        $this->inicialValue = self::VALUE_FOR_COINCIDENCES;
    }

    private function givenThatThereAreComplexCoincidences()
    {
        $this->inicialValue = self::VALUE_FOR_COMPLEX_COINCIDENCES;
    }

    private function whenGettingTheHttpRouterResult()
    {
        $service = new HttpRouter($this->routes);
        $this->result = $service->getResult($this->inicialValue);
    }

    private function thenTheResultShouldBeNull()
    {
        $this->assertSame(self::RESULT_FOR_NO_COINCIDENCES, $this->result);
    }

    private function thenTheResultShouldBeAnArray()
    {
        $this->assertInternalType('array',$this->result);
    }

    private function thenTheResultShouldBeOneElement()
    {
        $this->assertEquals(1,count($this->result));
    }

    private function thenTheRouteResultShouldBeTheSameAsInicial()
    {
        $ruta = key($this->result);
        $this->assertSame($this->inicialValue,$ruta);
    }

    private function thenTheResultShouldHaveRouteId()
    {
        $this->assertArrayHasKey('route_id', $this->result[$this->inicialValue]);
    }

    private function thenTheResultShouldHaveParameters()
    {
        $this->assertArrayHasKey('parameters', $this->result[$this->inicialValue]);
    }

    private function thenTheParametersShouldBeAnArray()
    {
        $this->assertInternalType('array',$this->result[$this->inicialValue]['parameters']);
    }


}