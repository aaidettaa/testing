<?php

namespace MpwarUnit\HttpRouterEric\Component\ParameterExtractor;

use PHPUnit_Framework_TestCase;
use Mpwar\HttpRouterEric\Component\ParameterExtractor\ExtractParametersFromPath;
use Mpwar\HttpRouterEric\Route;

final class ExtractParametersFromPathTest extends PHPUnit_Framework_TestCase
{
    protected function tearDown()
    {
        $this->givenPath = null;
        $this->route = null;
        $this->existingRoutes = null;
    }

    /**
     * @test
     * @dataProvider validCasesProvider
     */
    public function shouldModifyTheGivenRouteAcordingly(
        $path,
        $identifier,
        array $parameters
    ) {
        $this->givenAPath($path);
        $this->andARouteWithAnIdentifierAndPath($identifier, $path);
        $this->andASetOfExistingRoutes();
        $this->thenTheMatchingRouteWillHaveTheParameters($parameters);
    }

    public function validCasesProvider()
    {
        return [
            [
                'path' => 'simple-path',
                'identifier' => 'simple-path',
                'parameters' => [],
            ],
            [
                'path' => 'normal/path',
                'identifier' => 'normal_path',
                'parameters' => [],
            ],
            [
                'path' => 'path/with/one/parameter',
                'identifier' => 'path_with_one_parameter',
                'parameters' => ['one' => 'one'],
            ],
            [
                'path' => 'path/with/two/consecutive/parameters',
                'identifier' => 'path_with_two_consecutive_parameters',
                'parameters' => [
                    'with' => 'with',
                    'two' => 'two',
                ],
            ],
            [
                'path' => 'path/with/two/non-consecutive/parameters',
                'identifier' => 'path_with_two_non_consecutive_parameters',
                'parameters' => [
                    'with' => 'with',
                    'non_consecutive' => 'non-consecutive',
                ],
            ],
        ];
    }

    private function givenAPath($path)
    {
        $this->givenPath = $path;
    }

    private function andARouteWithAnIdentifierAndPath($identifier, $path)
    {
        $this->route = Route::toBeFilled()
            ->withIdentifier($identifier)
            ->withPath($path);
    }

    private function andASetOfExistingRoutes()
    {
        $this->existingRoutes = [
            'simple-path' => 'simple-path',
            'normal_path' => 'normal/path',
            'path_with_one_parameter' => 'path/with/{one}/parameter',
            'path_with_two_consecutive_parameters' => 'path/{with}/{two}/consecutive/parameters',
            'path_with_two_non_consecutive_parameters' => 'path/{with}/two/{non_consecutive}/parameters',
        ];
    }

    private function thenTheMatchingRouteWillHaveTheParameters(array $parameters)
    {
        $routeIdentifier = new ExtractParametersFromPath($this->existingRoutes);
        $route = $routeIdentifier->extractParametersFor($this->route, $this->givenPath);
        $extractedParameters = $route->parameters();

        foreach ($parameters as $expectedParameter => $expectedValue) {
            $this->assertArrayHasKey($expectedParameter, $extractedParameters);
            $this->assertEquals($expectedValue, $extractedParameters[$expectedParameter]);
        }
    }
}