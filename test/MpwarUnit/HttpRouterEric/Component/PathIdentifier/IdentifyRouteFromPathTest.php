<?php

namespace MpwarUnit\HttpRouterEric\Component\PathIdentifier;

use PHPUnit_Framework_TestCase;
use Mpwar\HttpRouterEric\Component\PathIdentifier\IdentifyRouteFromPath;
use Mpwar\HttpRouterEric\Route;

final class IdentifyRouteFromPathTest extends PHPUnit_Framework_TestCase
{
    protected function tearDown()
    {
        $this->givenPath = null;
        $this->existingRoutes = null;
    }

    /**
     * @test
     * @dataProvider knownPathsProvider
     */
    public function shouldModifyTheGivenRouteAcordingly($path, $identifier)
    {
        $this->givenAPath($path);
        $this->andASetOfExistingRoutes();
        $this->thenTheMatchingRouteHasTheIdentifier($identifier);
    }

    public function knownPathsProvider()
    {
        return [
            [
                'path' => 'simple-path',
                'identifier' => 'simple-path',
            ],
            [
                'path' => 'normal/path',
                'identifier' => 'normal_path',
            ],
            [
                'path' => 'path/with/one/parameter',
                'identifier' => 'path_with_one_parameter',
            ],
            [
                'path' => 'path/with/two/consecutive/parameters',
                'identifier' => 'path_with_two_consecutive_parameters',
            ],
            [
                'path' => 'path/with/two/non-consecutive/parameters',
                'identifier' => 'path_with_two_non_consecutive_parameters',
            ],
        ];
    }

    private function givenAPath($path)
    {
        $this->givenPath = $path;
    }

    private function andASetOfExistingRoutes()
    {
        $this->existingRoutes = [
            'simple-path' => 'simple-path',
            'normal_path' => 'normal/path',
            'path_with_one_parameter' => 'path/with/{one}/parameter',
            'path_with_two_consecutive_parameters' => 'path/{with}/{two}/consecutive/parameters',
            'path_with_two_non_consecutive_parameters' => 'path/{with}/two/{non-consecutive}/parameters',
        ];
    }

    private function thenTheMatchingRouteHasTheIdentifier($identifier)
    {
        $routeIdentifier = new IdentifyRouteFromPath($this->existingRoutes);
        $route = Route::toBeFilled();
        $route = $routeIdentifier->identifyPathFor($route, $this->givenPath);

        $this->assertSame($identifier, $route->id());
        $this->assertSame($this->givenPath, $route->path());
    }
}