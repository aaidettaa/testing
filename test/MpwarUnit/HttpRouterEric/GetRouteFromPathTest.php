<?php

namespace MpwarUnit\HttpRouterEric;

use PHPUnit_Framework_TestCase;
use Mpwar\HttpRouterEric\Component\RoutePathIdentifier;
use Mpwar\HttpRouterEric\Component\RouteParameterExtractor;
use Mpwar\HttpRouterEric\GetRouteFromPath;
use Mpwar\HttpRouterEric\Route;
use Mpwar\HttpRouterEric\UnrecognizedPath;

final class GetRouteFromPathTest extends PHPUnit_Framework_TestCase
{
    const UNKNOWN_PATH = 'unknown';
    const KNOWN_PATH = 'known';
    const ROUTE_WITH_IDENTIFIER = 'with identifier';
    const ROUTE_WITH_PARAMETERS = 'without identifier';

    protected function tearDown()
    {
        $this->givenPath = null;
        $this->obtainedRoute = null;
        $this->routeMatcher = null;
        $this->parameterExtractor = null;
    }

    /** @test */
    public function aPathCanBeUnrecognizable()
    {
        $this->givenAnUnknownPath();
        $this->thenAnErrorWillBeThrown();
        $this->whenGettingTheRouteFromThePath();
    }

    /** @test */
    public function aRouteShouldBeGenerated()
    {
        $this->givenAKnownPath();
        $this->afterResolvingTheRouteIdentifier();
        $this->andResolvingTheRouteParameters();
        $this->whenGettingTheRouteFromThePath();
        $this->thenARouteWithParametersIsObtained();
    }

    private function givenAnUnknownPath()
    {
        $this->givenPath = self::UNKNOWN_PATH;
        $emptyRoute = Route::toBeFilled();

        $this->routeMatcher = $this->getMock(RoutePathIdentifier::class);
        $this->routeMatcher->method('identifyPathFor')->will($this->returnValue($emptyRoute));

        $this->parameterExtractor = $this->getMock(RouteParameterExtractor::class);
    }

    private function givenAKnownPath()
    {
        $this->givenPath = self::KNOWN_PATH;
    }

    private function thenAnErrorWillBeThrown()
    {
        $this->setExpectedException(UnrecognizedPath::class);
    }

    private function afterResolvingTheRouteIdentifier()
    {
        $resolvedRoute = Route::toBeFilled()->withIdentifier(self::ROUTE_WITH_IDENTIFIER);

        $this->routeMatcher = $this->getMock(RoutePathIdentifier::class);
        $this->routeMatcher
            ->expects($this->once())
            ->method('identifyPathFor')
            ->with(
                $this->callback(function (Route $givenRoute) {
                    return $givenRoute->isUnknown();
                }),
                $this->givenPath
            )
            ->will($this->returnValue($resolvedRoute));
    }

    private function andResolvingTheRouteParameters()
    {
        $resolvedRoute = Route::toBeFilled()->withIdentifier(self::ROUTE_WITH_PARAMETERS);

        $this->parameterExtractor = $this->getMock(RouteParameterExtractor::class);
        $this->parameterExtractor
            ->expects($this->once())
            ->method('extractParametersFor')
            ->will($this->returnValue($resolvedRoute));
    }

    private function whenGettingTheRouteFromThePath()
    {
        $service = new GetRouteFromPath($this->routeMatcher, $this->parameterExtractor);
        $this->obtainedRoute = $service($this->givenPath);
    }

    private function thenARouteWithParametersIsObtained()
    {
        $this->assertSame(self::ROUTE_WITH_PARAMETERS, $this->obtainedRoute->id());
    }
}