<?php

namespace MpwarUnit\Money;

use Mpwar\Money\Money;
use PHPUnit_Framework_TestCase;

class MoneyTest extends PHPUnit_Framework_TestCase
{
    const MAXIMUM_NUMBER = 1000000;
    const NUMBER = 5784.29;
    private $initialNumber = null;
    const RESULT_FOR_GREATER_THAN_MAXIMUM = 0;

    protected function tearDown()
    {
        $this->initialNumber = null;
        $this->result = null;
    }

    /** @test */
    public function shouldReturnZeroWhenIsGreaterThanTheMaximum()
    {
        $this->givenAnyNumberGreaterThanTheMaximum();
        $this->whenGettingTheMoneyResult();
        $this->thenTheResultShouldBeZero();
    }
    /** @test */
    public function shouldReturnZeroWhenIsLessThanTheMaximum()
    {
        $this->givenAnyNumberLessThanTheMaximum();
        $this->whenGettingTheMoneyResult();
        $this->thenTheResultShouldBeAnArray();
    }

    private function givenAnyNumberGreaterThanTheMaximum()
    {
        $this->initialNumber = self::MAXIMUM_NUMBER + 100;
    }

    private function givenAnyNumberLessThanTheMaximum()
    {
        $this->initialNumber = self::NUMBER;
    }

    private function whenGettingTheMoneyResult()
    {
        $service = new Money();
        $this->result = $service->calculateResult($this->initialNumber);
    }

    private function thenTheResultShouldBeZero()
    {
        $this->assertSame(self::RESULT_FOR_GREATER_THAN_MAXIMUM, $this->result);
    }

    private function thenTheResultShouldBeAnArray()
    {
        $this->assertTrue(is_array($this->result),'It is not an array');
    }


}