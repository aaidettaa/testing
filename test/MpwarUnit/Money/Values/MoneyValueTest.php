<?php

namespace MpwarUnit\Money\Values;


use Mpwar\Money\Values\MoneyValue;
use PHPUnit_Framework_TestCase;

class MoneyValueTest extends PHPUnit_Framework_TestCase
{
    const NUMBER = 21;
    const RESULT_EXPECTED = 5;
    private $initialNumber = null;

    protected function tearDown()
    {
        $this->initialNumber = null;
        $this->result = null;
    }

    /** @test */
    public function shouldReturnHowManyTimesCanBeRepeated()
    {
        $this->givenAnyNumber();
        $this->whenGettingTheResult();
        $this->thenTheResultShouldBeANumberExpected();
    }

    private function givenAnyNumber()
    {
        print_r($this->initialNumber);
        $this->initialNumber = self::NUMBER;
    }

    private function whenGettingTheResult()
    {
        $service = new MoneyValue(4);
        $this->result = $service->howManyTimesCanBeRepeated($this->initialNumber);
    }

    private function thenTheResultShouldBeANumberExpected()
    {
        $this->assertSame(self::RESULT_EXPECTED, $this->result);
    }

}