<?php

namespace MpwarUnit\MoneyEric;

use PHPUnit_Framework_TestCase;
use Mpwar\MoneyEric\Decomposer;
use Mpwar\MoneyEric\DecomposingStep;
use Mpwar\MoneyEric\DecomposingStepsCollection;
use Mpwar\MoneyEric\Steps\GenericStep;

final class DecomposerTest extends PHPUnit_Framework_TestCase
{
    const AN_AMOUNT = 1234.56;
    const AN_STACKED_RESULT = 'stacked';

    protected function tearDown()
    {
        $this->decomposingSteps = null;
        $this->obtainedResult = null;
    }

    /** @test */
    public function shouldGetTheResultUsingAllTheDecomposingSteps()
    {
        $this->givenACollectionOfDecomposingSteps();
        $this->andUsingAllOfThem();
        $this->afterExecutingTheDecomposer();
        $this->thenTheResultWillBeTheOneComingFromTheLastOne();
    }

    private function givenACollectionOfDecomposingSteps()
    {
        $this->decomposingSteps = new DecomposingStepsCollection;
        $this->decomposingSteps->addStep($this->getMock(DecomposingStep::class));
        $this->decomposingSteps->addStep($this->getMock(DecomposingStep::class));
    }

    private function andUsingAllOfThem()
    {
        foreach ($this->decomposingSteps as $decomposingStep) {
            $decomposingStep
                ->expects($this->once())
                ->method('computeStackedResultFor')
                ->will($this->returnValue([
                    'remaining_amount' => self::AN_AMOUNT,
                    'stacked_result' => [self::AN_STACKED_RESULT],
                ]));
        }
    }

    private function afterExecutingTheDecomposer()
    {
        $decomposer = new Decomposer($this->decomposingSteps);
        $this->obtainedResult = $decomposer(self::AN_AMOUNT);
    }

    private function thenTheResultWillBeTheOneComingFromTheLastOne()
    {
        $this->assertSame([self::AN_STACKED_RESULT], $this->obtainedResult);
    }






    /** @test */
    public function neverWriteATestLikeThis()
    {
        $decomposingSteps = new DecomposingStepsCollection;
        $decomposingSteps->addStep(new GenericStep(500));
        $decomposingSteps->addStep(new GenericStep(200));
        $decomposingSteps->addStep(new GenericStep(100));
        $decomposingSteps->addStep(new GenericStep(50));
        $decomposingSteps->addStep(new GenericStep(20));
        $decomposingSteps->addStep(new GenericStep(10));
        $decomposingSteps->addStep(new GenericStep(5));
        $decomposingSteps->addStep(new GenericStep(2));
        $decomposingSteps->addStep(new GenericStep(1));
        $decomposingSteps->addStep(new GenericStep(0.5));
        $decomposingSteps->addStep(new GenericStep(0.2));
        $decomposingSteps->addStep(new GenericStep(0.1));
        $decomposingSteps->addStep(new GenericStep(0.05));
        $decomposingSteps->addStep(new GenericStep(0.02));
        $decomposingSteps->addStep(new GenericStep(0.01));

        $decomposer = new Decomposer($decomposingSteps);

        $expectedResultForTheFirstExample = [
            '500'  => 11,
            '200'  => 1,
            '50'   => 1,
            '20'   => 1,
            '10'   => 1,
            '2'    => 2,
            '0.2'  => 1,
            '0.05' => 1,
            '0.02' => 2,
        ];

        $this->assertSame($expectedResultForTheFirstExample, $decomposer(5784.29));

        $expectedResultForTheSecondExample = [
            '500'  => 2,
            '200'  => 1,
            '20'   => 1,
            '10'   => 1,
            '2'    => 2,
            '0.5'  => 1,
            '0.05' => 1,
            '0.01' => 1,
        ];

        $this->assertSame($expectedResultForTheSecondExample, $decomposer(1234.56));
    }
}