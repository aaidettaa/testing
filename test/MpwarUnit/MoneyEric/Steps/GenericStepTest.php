<?php

namespace MpwarUnit\MoneyEric\Steps;

use PHPUnit_Framework_TestCase;
use Mpwar\MoneyEric\Steps\GenericStep;

final class GenericStepTest extends PHPUnit_Framework_TestCase
{
    protected function tearDown()
    {
        $this->unitValue = null;
        $this->remainingAmount = null;
        $this->stackedResult = null;
        $this->obtainedResult = null;
    }

    /**
     * @test
     * @dataProvider stepsProvider
     */
    public function shouldObtainTheCorrectResult(
        $unitValue,
        $remainingAmount,
        array $stackedResult,
        array $expectedResult
    ) {
        $this->givenAUnitValueOf($unitValue);
        $this->andARemainingAmountOf($remainingAmount);
        $this->andAnStackedResult($stackedResult);
        $this->whenComputingTheStackedResult();
        $this->thenTheResultIsTheExpectedOne($expectedResult);
    }

    public function stepsProvider()
    {
        return [
            'Unit value smaller than the remaining amount' => [
                'unit_value' => 500,
                'remaining_amount' => 1643.0,
                'stacked_result' => [],
                'expected_result' => [
                    'remaining_amount' => 143.0,
                    'stacked_result' => ['500' => 3],
                ],
            ],
            'Unit value equals than the remaining amount' => [
                'unit_value' => 20,
                'remaining_amount' => 20.0,
                'stacked_result' => ['500' => 1],
                'expected_result' => [
                    'remaining_amount' => 0.0,
                    'stacked_result' => [
                        '500' => 1,
                        '20' => 1,
                    ],
                ],
            ],
            'Unit value bigger than the remaining amount' => [
                'unit_value' => 50,
                'remaining_amount' => 20.0,
                'stacked_result' => ['500' => 2],
                'expected_result' => [
                    'remaining_amount' => 20.0,
                    'stacked_result' => ['500' => 2],
                ],
            ],
            'Decimal unit' => [
                'unit_value' => 0.05,
                'remaining_amount' => 0.32,
                'stacked_result' => [],
                'expected_result' => [
                    'remaining_amount' => 0.02,
                    'stacked_result' => ['0.05' => 6],
                ],
            ],
            'With remaining decimals' => [
                'unit_value' => 2,
                'remaining_amount' => 6.31,
                'stacked_result' => [],
                'expected_result' => [
                    'remaining_amount' => 0.31,
                    'stacked_result' => ['2' => 3],
                ],
            ],
        ];
    }

    private function givenAUnitValueOf($unitValue)
    {
        $this->unitValue = $unitValue;
    }

    private function andARemainingAmountOf($remainingAmount)
    {
        $this->remainingAmount = $remainingAmount;
    }

    private function andAnStackedResult(array $stackedResult)
    {
        $this->stackedResult = $stackedResult;
    }

    private function whenComputingTheStackedResult()
    {
        $genericStep = new GenericStep($this->unitValue);
        $this->obtainedResult = $genericStep->computeStackedResultFor(
            $this->remainingAmount,
            $this->stackedResult
        );
    }

    private function thenTheResultIsTheExpectedOne(array $expectedResult)
    {
        $this->assertSame($expectedResult, $this->obtainedResult);
    }
}